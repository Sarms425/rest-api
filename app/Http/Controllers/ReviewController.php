<?php

namespace App\Http\Controllers;

use App\Model\Review;
use App\Model\Product;
use App\Http\Resources\Review\ReviewResource;
use Illuminate\Http\Request;
use App\Http\Requests\ReviewRequest;


class ReviewController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->except('index','show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return ReviewResource::collection($product->reviews);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReviewRequest $request,Product $product)
    {
        // $product_id=$product->id;
        // $data=$request->all();
        // $review=new Review;
        // $review->product_id=$product_id;
        // $review->customer=$data['customer'];
        // $review->review=$data['review'];
        // $review->star=$data['star'];
        // $review->save();
        $review=new Review($request->all());
        $product->reviews()->save($review);
        return response(['data'=>new ReviewResource($review)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Product $product,Review $review)
    {
        $review->update($request->all());
        return response(['data'=>new ReviewResource($review)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product,Review $review)
    {
        $review->delete();
        return response(['data'=>'Review data deleted successfully']);
    }
}
