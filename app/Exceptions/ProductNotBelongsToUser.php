<?php

namespace App\Exceptions;

use Exception;

class ProductNotBelongsToUser extends Exception
{
    public function render()
    {
        return ['errors'=>'Product does not belongs to the authenticated user'];
    }
}

