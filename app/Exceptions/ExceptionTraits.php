<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ExceptionTraits{
    public function apiException($request,$e)
    {
        if($this->isModel($e))
            {
                return response()->json(['errors'=>'Product model not found'],404);
            }
            if($this->isHttp($e))
            {
                return response()->json(['errors'=>'The given route is incorrect']);
            }
        return parent::render($request, $e);
    }
    protected function isModel($e)
    {
        return $e instanceof ModelNotFoundException;
    }
    protected function isHttp($e)
    {
        return $e instanceof NotFoundHttpException;
    }
}